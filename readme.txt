-------------------------------------------------------

Group AKA prototype implementation by:

Markus Ahlström & Simon Holmberg


-------------------------------------------------------

Source code can be found @ https://gitlab.eurecom.fr/groups/oai

How to install:

1. Download Openairinterface5G from the commit: 

	openairinterface5g:
	commit 5c9b40e1bbfedd069b67f63aa933058e18b7dda4
	Merge: 95e7010 7dabcfe
	Author: Rohit Gupta <rohit.gupta@eurecom.fr>
	Date:   Sun Feb 7 13:27:23 2016 +0100

2. Download openair-cn from the commit:

	commit 1f905b800d8d7398f974741be9c9cf3abe75ad18
	Merge: 1e590e2 d32cbff
	Author: gauthier <lionel.gauthier@eurecom.fr>
	Date:   Fri Feb 5 08:25:09 2016 +0100

3. Apply groupaka5g.patch to Openairinterface local repository

4. Apply groupakacn.patch to openair-cn local repository

5. Build OAI as specified in 
https://gitlab.eurecom.fr/oai/openair-cn/blob/2f301a23435b968d9a5ba4c0a06d082c63121c7a/DOCS/EPC_User_Guide.pdf.
and 
https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/HowtorunoaisimwithvirtualMMEonsamemachine


---------------------------------------------------------


How to run:

Case A of the protocol

In openair-cn

1. Run HSS

2. Run epc

In Openairinterface5G

1. run UE + EnB

Case B is performed during a second run

---------------------------------------------------------











